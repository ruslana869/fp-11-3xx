module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
--isKnown = error "isKnown"
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа

eval :: Term -> Int
--eval = error "eval"
eval (Const x) = x
eval x = case x of
  Add  a b -> eval a + eval b
  Sub  a b -> eval a - eval b
  Mult a b -> eval a * eval b
  
-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
--simplify = error "simplify"
simplify x = case x of 
  (Const a) -> Const a
  (Mult (Add a b) c) -> simplify (Add (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
  (Mult (Sub a b) c) -> simplify (Sub (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
  (Mult c (Add a b)) -> simplify (Add (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
  (Mult c (Sub a b)) -> simplify (Sub (Mult (simplify c) (simplify a)) (Mult (simplify c) (simplify b)))
  (Mult a b) -> Mult(simplify a) (simplify b)
  (Add a b) -> Add(simplify a) (simplify b)
  (Sub a b) -> Sub(simplify a) (simplify b)