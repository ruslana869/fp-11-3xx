-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"
--<<<<<<< HEAD
---- instance Show' A where
----   ???
--=======

--instance Show' A where
--  show' = undefined

--instance Show' C where
--  show' = undefined
-->>>>>>> remotes/gltronred/fp-11-3xx/master

instance Show' C where
    show' (C a b) = "C"++concatNums [a,b]
-- instance Show' A where
--   ???
instance Show' A where
    show' a = case a of
        A i -> "A"++concatNums [i]
        _ -> "B"

concatNums :: [Int] -> String
concatNums arr = foldl (\a b -> a++" "++b) "" $ map getNumber arr
    where getNumber i
           | i < 0 = "("++show i++")"
           | otherwise = show i
----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
--symmetricDifference = undefined
symmetricDifference (Set f1) (Set f2) = Set $ \elem -> (f1 elem || f2 elem) && not (f1 elem && f2 elem)


-----------------

fromBool bool
    | bool = \t f -> t
    | otherwise = \t f -> f

-- fromInt - переводит число в кодировку Чёрча
fromInt n
    | n == 0 = \s z -> z
    | otherwise = \s z -> s $ fromInt (n - 1) s z
