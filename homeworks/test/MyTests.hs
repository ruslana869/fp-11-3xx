-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests =   [testGroup "HWs"
                [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ],
            testGroup "test my_eval1" 
            	[ testCase "eval_1 (\\x . x) y == y" $ eval1 (App (Lam (Var 'x') (Var 'x')) (Var 'y')) @?= Just (Var 'y')
            	--testCase "Nothing case 1" $ eval1 (Lam (Lam (Var 'x') (Var 'y')) (Var 'v')) @?= Nothing
                       --testCase "Nothing case 2" $ eval_1 (Lam (App (Var 'x') (Var 'y')) (Var 'v')) @?= Nothing,
                       --testCase "eval_1 (\\x . x) y == y" $ eval_1 (App (Lam (Var 'x') (Var 'x')) (Var 'y')) @?= Just (Var 'y'),
                       --testCase "eval_1 (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                       --         $ eval_1 (App (Lam (Var 'x')
                       --                                (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                       --                        (Lam (Var 'z') (Var 'z')))
                       --                 @?= Just (Lam (Var 'y') (App (Lam (Var 'z') (Var 'z')) (Var 'y'))),
                       --testCase "eval_1 (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)"
                       --         $ eval_1 (App (App (Lam (Var 'x')
                       --                                (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                       --                        (Lam (Var 'z') (Var 'z')))
                       --                     (Lam (Var 'u') (Lam (Var 'v') (Var 'v'))))
                       --                 @?= Just (App (Lam (Var 'y') (App (Lam (Var 'z') (Var 'z')) (Var 'y')))
                       --                               (Lam (Var 'u') (Lam (Var 'v') (Var 'v')))),
                       --testCase "eval_1 (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                       --         $ eval_1 (Lam (Var 'x') (App (Lam (Var 'x') (Var 'x')) (Var 'x')))
                       --             @?= Just (Lam (Var 'x') (App (Lam (Var 'x') (Var 'x')) (Var 'x'))),
                       --testCase "eval1 (\\x . \\y . x) y = (\\y . a)"
                       --         $ eval1 (App (Lam (Var 'x') (Lam (Var 'y') (Var 'x'))) (Var 'y'))
                       --             @?= Just (Lam (Var 'y') (Var 'a'))
                 ],
            testGroup "test my_eval" [
                 --      testCase "eval_ (\\x . x) y == y" $ eval_ (App (Lam (Var 'x') (Var 'x')) (Var 'y')) @?= (Var 'y'),
                 --      testCase "eval_ (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                 --               $ eval_ (App (Lam (Var 'x')
                 --                                      (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                 --                              (Lam (Var 'z') (Var 'z')))
                 --                       @?= Lam (Var 'y') (App (Lam (Var 'z') (Var 'z')) (Var 'y')),
                 --      testCase "eval_ (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)"
                 --               $ eval_ (App (App (Lam (Var 'x')
                 --                                      (Lam (Var 'y') (App (Var 'x') (Var 'y'))))
                 --                              (Lam (Var 'z') (Var 'z')))
                 --                           (Lam (Var 'u') (Lam (Var 'v') (Var 'v'))))
                 --                       @?= Lam (Var 'u') (Lam (Var 'v') (Var 'v')),
                 --      testCase "eval_ (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                 --               $ eval_ (Lam (Var 'x') (App (Lam (Var 'x') (Var 'x')) (Var 'x')))
                 --                   @?= Lam (Var 'x') (App (Lam (Var 'x') (Var 'x')) (Var 'x')),
                 --      testCase "eval_ (\\x . \\y . x) y = (\\y . a)"
                 --               $ eval_ (App (Lam (Var 'x') (Lam (Var 'y') (Var 'x'))) (Var 'y'))
                 --                   @?= Lam (Var 'y') (Var 'a')
                 ]
            ]

