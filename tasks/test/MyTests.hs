-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests =   [testGroup "HWs"
                [ testCase "primeFactor" $ hw0_0 "13195" @?= "29" ],
                       ]

