import MyTests (myTests)

main :: IO ()
main = defaultMainWithIngredients [antXMLRunner, consoleTestReporter, listingTests] tests

tests :: TestTree
tests = testGroup "Tests"
        [ testGroup "User defined tests" myTests
        ]
